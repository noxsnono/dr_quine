#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/04/02 15:15:21 by jmoiroux          #+#    #+#              #
#    Updated: 2015/04/02 15:15:25 by jmoiroux         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC = $(CLANG)

COLLEEN_NAME = Colleen
GRACE_NAME = Grace
SULLY_NAME = Sully

COLLEEN_SRCS =	Colleen.c
GRACE_SRCS =	Grace.c
SULLY_SRCS =	Sully.c

COLLEEN_OBJS = $(COLLEEN_SRCS:.c=.o)
GRACE_OBJS = $(GRACE_SRCS:.c=.o)
SULLY_OBJS = $(SULLY_SRCS:.c=.o)

GCC = gcc
CLANG = clang
CFLAGS =

%.o: %.c
	@$(CC) $(CFLAGS) -c $< -o $@

all: $(COLLEEN_NAME) $(GRACE_NAME) $(SULLY_NAME)

$(COLLEEN_NAME): $(COLLEEN_OBJS)
	$(CC) $(CFLAGS) $(COLLEEN_OBJS) -o $(COLLEEN_NAME)

$(GRACE_NAME): $(GRACE_OBJS)
	$(CC) $(CFLAGS) $(GRACE_OBJS) -o $(GRACE_NAME)

$(SULLY_NAME): $(SULLY_OBJS)
	$(CC) $(CFLAGS) $(SULLY_OBJS) -o $(SULLY_NAME)

clean:
	rm -rf $(COLLEEN_OBJS)
	rm -rf $(GRACE_OBJS)
	rm -rf $(SULLY_OBJS)

fclean: clean
	rm -rf $(COLLEEN_NAME)
	rm -rf $(GRACE_NAME)
	rm -rf $(SULLY_NAME)

re: fclean all

.PHONY: all clean fclean re
